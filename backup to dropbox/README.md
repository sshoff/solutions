# Backup to Dropbox: with or without encryption

## Backup without encryption

All commands should be run on a server.

First of all download dropbox_uploader.sh from [GitHub](https://github.com/andreafabrizi/Dropbox-Uploader):

```bash
curl "https://raw.githubusercontent.com/andreafabrizi/Dropbox-Uploader/master/dropbox_uploader.sh" -o dropbox_uploader.sh
```

Make the script executable:
```bash
chmod +x dropbox_uploader.sh
```

Run the script:
```bash
./dropbox_uploader.sh
```

Now you need an App key from Dropbox. Open on your local machine a browser and go to https://www.dropbox.com/developers/apps . Create an app which works with Dropbox API. In opened app's Settings click button "Generate" in "Oauth2 / Generated access token". That's needed key! Enter this key on a server which waiting for its.

Now test how does it works:
```bash
./dropbox_uploader.sh upload dropbox_uploader.sh remote_folder/
```

Open your Dropbox account in a web browser on a local machine and you should find uploaded file `dropbox_uploader.sh` in `remote_folder` folder.

If it is, great, everything working fine!

## Creating a cron job

The original version of the script you can find at [GitHub](https://github.com/andreafabrizi/Dropbox-Uploader/wiki/Simple-backup-script).

My version with preparing for encryption and removing old files you can find here: <<file backup-to-dropbox.sh>>.

If the variable `PASSWORD` is set to empty string then the backup will be without encryption. This is behavior by default.

Make the script executable:
```bash
chmod +x backup-to-dropbox.sh
```

Add a task to cron:
```bash
crontab -e
```
Insert:
```bash
00 4 * * *     /path/to/backup-to-dropbox.sh 2>&1 >> /var/log/backup-to-dropbox.log
```

## Enabling encryption

Install 7zip on your server:
```bash
apt-get install p7zip p7zip-full
```

Set password for archive in variable `PASSWORD` in the script `backup-to-dropbox.sh`.