#!/bin/bash

###
#
# Variables
#
###

# Password for an encrypted archive. If not set archive will not be encrypted
PASSWORD=""

# Absolute paths to directories that should be backup
BKP_DIRS="/home/user /var/www /etc"
# A directory where will be stored temp files
TMP_DIR="/tmp"
# Current date
DATE=$(date +"%Y%m%d")
# A period of maximum storing backups in Dropbox
DELDATE=`date --date="-30 day" +%Y%m%d`
# A prefix to archive file name
BKP_PREFIX="your_project_name"
# A name of a backup file
BKP_FILE="$TMP_DIR/$BKP_PREFIX-$DATE.tar"

# Path to a dropbox_uploader file
DROPBOX_UPLOADER=/root/dropbox_uploader.sh
# Name of a directory in Dropbox 
DROPBOX_UPLOAD_DIR="dropbox_upload_dir_name"

###
#
# Start of the script
#
###

tar cf "$BKP_FILE" $BKP_DIRS

if [[ -z "$PASSWORD" ]]; then
    ## Working without an encryption

    # Filename extension of archive file
    BKP_ARCH_FILENAME_EXTANSION="gz"

    ## Archive without encryption
    gzip "$BKP_FILE"

else
    ## Use an encryption
    
    # Filename extension of archive file
    BKP_ARCH_FILENAME_EXTANSION="7z"

    ## Archive with an encryption
    # https://askubuntu.com/questions/928275/7z-command-line-with-highest-encryption-aes-256-encrypting-the-filenames
    # a                   Add (dir1 to archive.7z)
    # -t7z                Use a 7z archive
    # -m0=lzma2           Use lzma2 method
    # -mx=9               Use the '9' level of compression = Ultra
    # -mfb=64             Use number of fast bytes for LZMA = 64
    # -md=32m             Use a dictionary size = 32 megabytes
    # -ms=on              Solid archive = on
    # -mhe=on             7z format only : enables or disables archive header encryption
    # -p{Password}        Add a password
    #
    # Show archive info: 7z l -slt archive.7z
    7z a -t7z -m0=lzma2 -mx=9 -mfb=64 -md=32m -ms=on -mhe=on -p"$PASSWORD" "$BKP_FILE.$BKP_ARCH_FILENAME_EXTANSION" "$BKP_FILE"

    rm "$BKP_FILE"
fi

# Home directory of current user
HOME_DIR=$( getent passwd "$USER" | cut -d: -f6 )

$DROPBOX_UPLOADER -f "$HOME_DIR/.dropbox_uploader" upload "$BKP_FILE.$BKP_ARCH_FILENAME_EXTANSION" $DROPBOX_UPLOAD_DIR/

rm -fr "$BKP_FILE.$BKP_ARCH_FILENAME_EXTANSION"

$DROPBOX_UPLOADER -f "$HOME_DIR/.dropbox_uploader" delete "$DROPBOX_UPLOAD_DIR/$BKP_PREFIX-${DELDATE}.tar.$BKP_ARCH_FILENAME_EXTANSION"
